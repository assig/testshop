var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic(__dirname)).listen(9000);
// to start server
// node Gruntfile.js

module.exports = function(grunt){

  require("load-grunt-tasks")(grunt);

  grunt.initConfig({

    //svgstore: {
    //  options: {
    //    prefix : "icon-"
    //  },
    //  default : {
    //    files: {
    //      "static/img/sprite.svg": ["static/img/**/*.svg"]
    //    }
    //  }
    //},

    "bitbucket-pages": {
      publish: {
        options: {
          repository: "https://tufedtm@bitbucket.org/tufedtm/tufedtm.bitbucket.org.git",
          siteName: "testshop"
        },
        src: ["!node_modules", "*.html", "static/**/*.css", "static/font", "static/img", "static/**/*.js"]
      }
    },

    //webfont: {
    //  icons: {
    //    src: 'static/img/**/*.svg',
    //    dest: 'static/font/ico',
    //    options: {
    //      stylesheet: 'less',
    //      types: 'eot,ttf,svg,woff'
    //    }
    //  }
    //},

    //csscomb: {
    //  style: {
    //    expand: true,
    //    src: ["static/less/**/*.less"]
    //  }
    //},

    less: {
      style: {
        files: {
          "static/css/style.css": ["static/less/style.less"]
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ["last 2 versions", "ie 10"]
      },
      files: {
        src: "static/css/style.css"
      }
    },

    cmq: {
      style: {
        files: {
          "static/css/style.css": ["static/css/style.css"]
        }
      }
    },

    cssmin: {
      style: {
        options: {
          keepSpecialComments: 0,
          report: "gzip"
        },
        files: {
          "static/css/style.min.css": ["static/css/style.css"]
        }
      }
    },

    concat: {
      script: {
        src: ["static/js/**/*.js", "!static/js/script.js", "!static/js/script.min.js"],
        dest: "static/js/script.js"
      }
    },

    uglify: {
      options: {
        report: "gzip"
      },
      script: {
        src: "static/js/script.js",
        dest: "static/js/script.min.js"
      }
    },

    watch: {
      options: {
        livereload: true,
        spawn: false
      },
      style: {
        files: ["static/less/**/*.less"],
        tasks: ["less", "autoprefixer", "cmq", "cssmin"]
      },
      script: {
        files: ["static/js/**/*.js"],
        tasks: ["concat", "uglify"]
      },
      templates: {
        files: ["**/*.html"]
      }
    }
  });

  //grunt.registerTask("default", [
  //  "less",
  //  "autoprefixer",
  //  "cmq",
  //  "cssmin",
  //  "concat",
  //  "uglify"
  //]);
};
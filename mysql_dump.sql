-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 14 2016 г., 18:10
-- Версия сервера: 5.5.46-0ubuntu0.14.04.2
-- Версия PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add Категория', 7, 'add_category'),
(20, 'Can change Категория', 7, 'change_category'),
(21, 'Can delete Категория', 7, 'delete_category'),
(22, 'Can add Подкатегория', 8, 'add_subcategory'),
(23, 'Can change Подкатегория', 8, 'change_subcategory'),
(24, 'Can delete Подкатегория', 8, 'delete_subcategory'),
(25, 'Can add Товар', 9, 'add_product'),
(26, 'Can change Товар', 9, 'change_product'),
(27, 'Can delete Товар', 9, 'delete_product');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$24000$FqBxbRcPoHac$j12ehaQ8M/EJyGa8gBP6QIHM9fpL7nN743jeJz/W1ho=', '2016-06-13 23:37:25', 1, 'admin', '', '', '', 1, 1, '2016-06-13 23:37:09');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2016-06-13 23:37:52', '1', 'Телефоны', 1, 'Добавлено.', 7, 1),
(2, '2016-06-14 00:29:07', '2', 'Комплектующие для компьютера', 1, 'Добавлено.', 7, 1),
(3, '2016-06-14 00:29:48', '1', 'Iphone', 1, 'Добавлено.', 8, 1),
(4, '2016-06-14 00:30:01', '2', 'Samsung', 1, 'Добавлено.', 8, 1),
(5, '2016-06-14 00:30:20', '3', 'Процессоры', 1, 'Добавлено.', 8, 1),
(6, '2016-06-14 00:30:31', '4', 'Материнские платы', 1, 'Добавлено.', 8, 1),
(7, '2016-06-14 00:34:37', '1', 'Процессор Intel® Core™ i3-6100 OEM <3.7G', 1, 'Добавлено.', 9, 1),
(8, '2016-06-14 00:36:07', '2', 'Процессор Intel® Core™ i5-6500 OEM <3.2GHz, 6Mb, LGA1151, Skylake>', 1, 'Добавлено.', 9, 1),
(9, '2016-06-14 00:38:32', '3', 'Мат. плата ASUS Z97-K <S1150, iZ97, 4*DDR3, 2*PCI-E16x, SVGA, DVI, HDMI, SATA III, USB 3.0, GB Lan>', 1, 'Добавлено.', 9, 1),
(10, '2016-06-14 00:41:03', '4', 'Apple iPhone 6 Plus 128GB Silver (MGAE2RU/A)', 1, 'Добавлено.', 9, 1),
(11, '2016-06-14 00:42:59', '5', 'Смартфон Apple iPhone 5S 16Gb Space Gray (ME432RU/A)', 1, 'Добавлено.', 9, 1),
(12, '2016-06-14 00:43:38', '5', 'Apple iPhone 5S 16Gb Space Gray (ME432RU/A)', 2, 'Изменен title и slug.', 9, 1),
(13, '2016-06-14 00:44:40', '6', 'Apple iPhone 6s Plus 16GB Silver (MKU22RU/A)', 1, 'Добавлено.', 9, 1),
(14, '2016-06-14 00:46:01', '7', 'Samsung Galaxy A5 (2016) SM-A510F Black', 1, 'Добавлено.', 9, 1),
(15, '2016-06-14 15:09:08', '4', '222', 3, '', 7, 1),
(16, '2016-06-14 15:09:08', '3', '11', 3, '', 7, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(7, 'products', 'category'),
(9, 'products', 'product'),
(8, 'products', 'subcategory'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Структура таблицы `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2016-06-13 23:36:18'),
(2, 'auth', '0001_initial', '2016-06-13 23:36:22'),
(3, 'admin', '0001_initial', '2016-06-13 23:36:22'),
(4, 'admin', '0002_logentry_remove_auto_add', '2016-06-13 23:36:23'),
(5, 'contenttypes', '0002_remove_content_type_name', '2016-06-13 23:36:23'),
(6, 'auth', '0002_alter_permission_name_max_length', '2016-06-13 23:36:23'),
(7, 'auth', '0003_alter_user_email_max_length', '2016-06-13 23:36:24'),
(8, 'auth', '0004_alter_user_username_opts', '2016-06-13 23:36:24'),
(9, 'auth', '0005_alter_user_last_login_null', '2016-06-13 23:36:24'),
(10, 'auth', '0006_require_contenttypes_0002', '2016-06-13 23:36:24'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2016-06-13 23:36:24'),
(12, 'products', '0001_initial', '2016-06-13 23:36:26'),
(13, 'sessions', '0001_initial', '2016-06-13 23:36:27'),
(14, 'products', '0002_auto_20160614_0332', '2016-06-14 00:32:43');

-- --------------------------------------------------------

--
-- Структура таблицы `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('jad1l0a8bjsrt30cisxuik42gswc0bwm', 'M2ZjNmE1YTc2ZmFjM2EyM2JhMjlhYTgzOWViMThmNzFmYWI3ZmM0Mzp7Il9hdXRoX3VzZXJfaGFzaCI6ImI3MzFmNDQ5MWU4YmM3ZWRjYmM3MDE5ZDRmNTA4NTdlZWZkNjE1NWMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-06-27 23:37:25');

-- --------------------------------------------------------

--
-- Структура таблицы `products_category`
--

CREATE TABLE `products_category` (
  `id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `slug` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products_category`
--

INSERT INTO `products_category` (`id`, `title`, `slug`) VALUES
(1, 'Телефоны', 'telefony'),
(2, 'Комплектующие для компьютера', 'komplektuyushie-dlya-kompyutera');

-- --------------------------------------------------------

--
-- Структура таблицы `products_product`
--

CREATE TABLE `products_product` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(40) NOT NULL,
  `description` longtext NOT NULL,
  `img` varchar(100) NOT NULL,
  `price` smallint(5) UNSIGNED NOT NULL,
  `subcategory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products_product`
--

INSERT INTO `products_product` (`id`, `title`, `slug`, `description`, `img`, `price`, `subcategory_id`) VALUES
(1, 'Процессор Intel® Core™ i3-6100 OEM <3.7G', 'processor-intel-core-i3-6100-oem-37g', 'В качестве ядра в представленной Intel Core i3-6100 OEM используется версия Skylake. Процессорный разъем - Socket 1151. Тактовая частота работы процессора составляет 3,7 ГГц, частота системной шины равна 8000 МГц, что на ряду с примененной в модели четырехядерной схемой дает отличные показатели быстродействия и работоспособности. Поддерживаемые стандарты оперативной памяти – DDR 3 и DDR 4, а ее частота составляет 2133 МГц. Конструкцией предусматривается встроенное графическое ядро HD Graphics 530. Показатель тепловыделения равен всего 47 Вт. На рынок устройство поставляется в стандартной компактной упаковке типа OEM, габариты которой составляют 40*40*5 мм, а вес – 0,026 кг.', 'images/Комплектующие для компьютера/Процессоры/Процессор_Intel_Core_i3-6100_OEM_3.7G', 8863, 3),
(2, 'Процессор Intel® Core™ i5-6500 OEM <3.2GHz, 6Mb, LGA1151, Skylake>', 'processor-intel-core-i5-6500-oem-32ghz-6', 'Intel Core i5-6500 является прекрасным решением для пользователей любого уровня, она может быть использована как в домашнем компьютере, так и в офисной системе. В конструкции используется ядро Skylake и процессорный разъем Socket 1151.Тактовая частота составляет 3,2 ГГц, а частота системной шины – 8000 МГц. Процессор является четырехядерным, что заметно отражается на быстродействии всех выполняемых процессов и задач. Поддерживаемые стандарты оперативной памяти – DDR 3 и DDR 4. Конструкция включает в себя встроенное графическое ядро HD Graphics 530. Тепловыделение модели в обычных условиях составляет 65 Вт и с ним без труда справится система охлаждения вашей системы. Устройство поставляется в компактной упаковке стандарта OEM, имеет габариты 40*40*5 мм и вес 0,025 кг.', 'images/Комплектующие для компьютера/Процессоры/Процессор_Intel_Core_2niawkJ.2GHz_6Mb_LGA1151_Skylake', 15695, 3),
(3, 'Мат. плата ASUS Z97-K <S1150, iZ97, 4*DDR3, 2*PCI-E16x, SVGA, DVI, HDMI, SATA III, USB 3.0, GB Lan>', 'mat-plata-asus-z97-k-s1150-iz97-4ddr3-2p', 'ASUS Z97-K позволяет контролировать сетевую активность в режиме реального времени с помощью технологии TurboLAN. Она также позволяет установить приоритет для каждой программы на доступ к ресурсам сети.\r\n\r\nДанная модель также оснащена интерфейсом M.2, пропускная способность которого составляет 10 Гбит/с.', 'images/Комплектующие для компьютера/Материнские платы/Мат._плата_ASUS_Z97-K_S1150_i_oQ2EGAS.0_GB_Lan', 7696, 4),
(4, 'Apple iPhone 6 Plus 128GB Silver (MGAE2RU/A)', 'apple-iphone-6-plus-128gb-silver-mgae2ru', 'Смартфон Apple iPhone 6 Plus 128GB Silver (MGAE2RU/A) с 5,5-дюймовым сенсорным экраном построен на базе 64-битного процессора А8, усиленного сопроцессором М8. Модель iPhone 6 Plus поддерживает функцию голосового управления Siri и технологию биометрической защиты Touch ID.', 'images/Телефоны/Iphone/Apple iPhone 6 Plus 128GB Silver (MGAE2RU/A', 59990, 1),
(5, 'Apple iPhone 5S 16Gb Space Gray (ME432RU/A)', 'apple-iphone-5s-16gb-space-gray', 'Процессор A7 с 64-битной архитектурой. Датчик идентификации по отпечатку пальца. Усовершенствованная камера, которая стала ещё быстрее. Операционная система, разработанная специально для 64-битной архитектуры. Любая из этих функций позволила бы смартфону быть на шаг впереди других. А со всеми этими функциями iPhone просто опережает своё время.', 'images/Телефоны/Iphone/Смартфон Apple iPhone 5S 16Gb Space Gray (ME432RU/A', 22990, 1),
(6, 'Apple iPhone 6s Plus 16GB Silver (MKU22RU/A)', 'apple-iphone-6s-plus-16gb-silver-mku22ru', 'Apple iPhone 6s Plus оснащён обновлённым экраном Retina с разрешением Full HD, характерными особенностями которого являются высокая детализация изображения, яркость и чёткость картинки, широкие углы обзора. Безопасность внутренних компонентов девайса обеспечивается благодаря применению корпуса из сверхпрочного алюминиевого сплава, а также закалённого стекла на лицевой панели.', 'images/Телефоны/Iphone/Apple iPhone 6s Plus 16GB Silver (MKU22RU/A', 59990, 1),
(7, 'Samsung Galaxy A5 (2016) SM-A510F Black', 'samsung-galaxy-a5-2016-sm-a510f-black', 'Тонкий, лёгкий корпус из стекла и металла – воплощение безупречного стиля. Оцените, как роскошно Samsung Galaxy A смотрится и как удобно лежит в руке.', 'images/Телефоны/Samsung/Samsung_Galaxy_A5_2016_SM-A510F_Black', 23990, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `products_subcategory`
--

CREATE TABLE `products_subcategory` (
  `id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `slug` varchar(40) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products_subcategory`
--

INSERT INTO `products_subcategory` (`id`, `title`, `slug`, `category_id`) VALUES
(1, 'Iphone', 'iphone', 1),
(2, 'Samsung', 'samsung', 1),
(3, 'Процессоры', 'processory', 2),
(4, 'Материнские платы', 'materinskie-platy', 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`);

--
-- Индексы таблицы `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Индексы таблицы `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Индексы таблицы `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Индексы таблицы `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`);

--
-- Индексы таблицы `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Индексы таблицы `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Индексы таблицы `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_de54fa62` (`expire_date`);

--
-- Индексы таблицы `products_category`
--
ALTER TABLE `products_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_2dbcba41` (`slug`);

--
-- Индексы таблицы `products_product`
--
ALTER TABLE `products_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_product_2dbcba41` (`slug`),
  ADD KEY `products_product_79f70305` (`subcategory_id`);

--
-- Индексы таблицы `products_subcategory`
--
ALTER TABLE `products_subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_subcategor_category_id_44d297b7_fk_products_category_id` (`category_id`),
  ADD KEY `products_subcategory_2dbcba41` (`slug`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `products_category`
--
ALTER TABLE `products_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `products_product`
--
ALTER TABLE `products_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `products_subcategory`
--
ALTER TABLE `products_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `products_product`
--
ALTER TABLE `products_product`
  ADD CONSTRAINT `products_prod_subcategory_id_b28a1e3b_fk_products_subcategory_id` FOREIGN KEY (`subcategory_id`) REFERENCES `products_subcategory` (`id`);

--
-- Ограничения внешнего ключа таблицы `products_subcategory`
--
ALTER TABLE `products_subcategory`
  ADD CONSTRAINT `products_subcategor_category_id_44d297b7_fk_products_category_id` FOREIGN KEY (`category_id`) REFERENCES `products_category` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

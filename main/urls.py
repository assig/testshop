from django.conf.urls import url
from . import views as main_views
from products import views as products_views

urlpatterns = [
    url(r'^$', main_views.home, name='home'),

    url(r'^add$', products_views.add, name='add'),
    url(r'^add/(?P<add_item_url>[\w-]+)$', products_views.add_item, name='add_item'),

    url(r'^cart$', main_views.cart, name='cart'),
    url(r'^cart/add_item$', main_views.add_item, name='add_cart_item'),
    url(r'^cart/delete_item$', main_views.del_item, name='del_cart_item'),
    url(r'^cart/change_item_count$', main_views.change_item_count, name='change_cart_item_count'),

    url(r'^(?P<category_slug>[\w-]+)$', products_views.category, name='category'),
    url(r'^(?P<category_slug>[\w-]+)/(?P<subcategory_slug>[\w-]+)$', products_views.subcategory, name='subcategory'),
    url(r'^(?P<category_slug>[\w-]+)/(?P<subcategory_slug>[\w-]+)/(?P<product_slug>[\w-]+)$', products_views.product,
        name='product'),
]

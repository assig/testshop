from django import forms
from .models import Category, Subcategory, Product


class BaseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class CategoryForm(BaseForm):
    class Meta:
        model = Category
        fields = '__all__'


class SubcategoryForm(BaseForm):
    class Meta:
        model = Subcategory
        fields = '__all__'


class ProductForm(BaseForm):
    class Meta:
        model = Product
        fields = '__all__'

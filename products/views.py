# coding: utf-8
from django.shortcuts import render, get_object_or_404
from .models import Category, Subcategory, Product
from .forms import CategoryForm, SubcategoryForm, ProductForm


def category(request, category_slug):
    category_item = get_object_or_404(Category, slug=category_slug)
    products = Product.objects.filter(subcategory__category__slug=category_slug)
    context = {'category_item': category_item, 'products': products, 'category_active': category_slug,
               'subcategory_active': 'all'}
    return render(request, 'products.html', context)


def subcategory(request, category_slug, subcategory_slug):
    subcategory_item = get_object_or_404(Subcategory, slug=subcategory_slug)
    products = Product.objects.filter(subcategory__slug=subcategory_slug)
    context = {'subcategory_item': subcategory_item, 'products': products, 'category_active': category_slug,
               'subcategory_active': subcategory_slug}
    return render(request, 'products.html', context)


def product(request, category_slug, subcategory_slug, product_slug):
    product_item = get_object_or_404(Product, slug=product_slug)
    context = {'product_item': product_item, 'category_active': category_slug, 'subcategory_active': subcategory_slug}
    return render(request, 'product.html', context)


def add(request):
    return render(request, 'add_items/add.html', {'add_item_url': 'active'})


def add_item(request, add_item_url):
    if request.method == "POST":
        if add_item_url == 'category':
            form = CategoryForm(request.POST)
        elif add_item_url == 'subcategory':
            form = SubcategoryForm(request.POST)
        else:
            form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            success = 'success'
        else:
            context = {'form': form, 'add_item_title': 'Ошибка при добавлении',
                       'add_item_url': add_item_url, 'success': ''}
            return render(request, 'add_items/add_item.html', context)
    else:
        success = ''

    if add_item_url == 'category':
        form = CategoryForm()
        add_item_title = "Добавление Категории"
    elif add_item_url == 'subcategory':
        form = SubcategoryForm()
        add_item_title = "Добавление Подкатегории"
    else:
        form = ProductForm()
        add_item_title = "Добавление Товара"
    context = {'form': form, 'add_item_title': add_item_title, 'add_item_url': add_item_url, 'success': success}
    return render(request, 'add_items/add_item.html', context)

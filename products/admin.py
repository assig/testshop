from django.contrib import admin
from .models import Category, Subcategory, Product


class SlugAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Category, SlugAdmin)
admin.site.register(Subcategory, SlugAdmin)
admin.site.register(Product, SlugAdmin)

from .models import Category, Subcategory


def categories(request):
    context = {'categories': Category.objects.all(), 'subcategories': Subcategory.objects.all()}
    return context

from io import BytesIO
from PIL import Image
from django.core.files.base import ContentFile
from stdimage.utils import render_variations


def preprocess(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format
            image.thumbnail((1920, 1200), resample=Image.ANTIALIAS)

            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                storage.delete(file_name)
                storage.save(file_name, f)

    render_variations(file_name, variations, replace=True, storage=storage)
    return False

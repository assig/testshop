# coding: utf-8
from __future__ import unicode_literals

from django.db import models
from stdimage import StdImageField
from stdimage.utils import pre_delete_delete_callback, pre_save_delete_callback
from django.db.models.signals import post_delete, pre_save
from products.max_image_size import preprocess


class Category(models.Model):
    title = models.CharField("Название", max_length=40)
    slug = models.SlugField("Нащвание в url", max_length=40)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __unicode__(self):
        return self.title


class Subcategory(models.Model):
    title = models.CharField("Название", max_length=40)
    slug = models.SlugField("Нащвание в url", max_length=40)
    category = models.ForeignKey(Category, verbose_name="Категория", related_name='subcategories')

    class Meta:
        verbose_name = "Подкатегория"
        verbose_name_plural = "Подкатегории"

    def __unicode__(self):
        return self.title


def image_path(instance, filename):
    return 'images/{0}/{1}/{2}'.format(instance.subcategory.category.title, instance.subcategory.title, instance.title)


class Product(models.Model):
    title = models.CharField("Название", max_length=100)
    slug = models.SlugField("Нащвание в url", max_length=40)
    description = models.TextField("Описание", max_length=1000)
    img = StdImageField("Изображение", upload_to=image_path, variations={'thumb': (200, 300, True),},
                        render_variations=preprocess)
    price = models.PositiveSmallIntegerField("Цена")
    subcategory = models.ForeignKey(Subcategory, verbose_name="Подкатегория", related_name='products')

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    def __unicode__(self):
        return self.title

post_delete.connect(pre_delete_delete_callback, sender=Product)
pre_save.connect(pre_save_delete_callback, sender=Product)
